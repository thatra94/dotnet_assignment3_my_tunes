# Dotnet assignment3 MyTunes

Assigment for Noroff Dotnet programme. The Purpose of the project is to create a rest api with a database where you can Get, Post and Put.
It is made with C#, ASP.NET and MVC(Model-View-Controller) pattern.

##### By Thanh Tran and Olav Rongved

## Starting the program

**Database**

We have used the database Chinook for this assignment. It can be found in 

`Resources/DB/Chinook_SqlServer_AutoIncrementPKs.sql`

Open it and create it in your database program, we have used Microsoft SQL Server Management Studio and SQL Express.

To connect to your local database change the datasource to your local database name in the class

`MyTunes/Repositories/ConnectionHelper.cs` 

**Program**

Start the server in a code editor like Visual Studio. 

Then you can either test the endpoints in Postman by importing the postman collection in 

`Resources/Postman/MyTunes.postman_collection.json`

Or test the endpoints in your browser. 

## Endpoints

**Customers**

`Root url https://localhost:44383/`

```
GET api/customers
GET api/customers/{id}
GET api/customers?country={country}
GET api/customers/countries
GET api/customers/payments
GET api/customers/{id}/genres/popular
POST api/customers
PUT api/customers/{id}
```

**Music**

```
GET api/music/{id}
GET api/music/genre/{id}
GET api/music/artist/{id}
GET api/music/home
GET api/music/search?track={track}
```

## Assignment requirements

**Music**

One endpoint that gets 

- [x] 5 random artists
- [x] 5 random songs
- [x] 5 random genres

---
**Results**

- [x] Shows the query the user has made
- [x] Search results shows a row where the track name, artist, album and genre are shown (Get api/music/search?track=foo)

---
**Customers**

Read all the customers in the database, display their:

- [x] ID
- [x] Last name
- [x] Country
- [x] Postal code
- [x] Phone number
- [x] Email
---
- [x] Add new customer to database, only the fields listed above
- [x] Update an existing customer
- [x] Return the number of customers in each country ordered descending(most number of customers to least)
- [x] Customers who are the highest spenders(total in invoice table is the largest), ordered descending
- [x] For a given customer, most popular genre(if tie, display both). Most popular means the genre with most tracks from invocies associated to that customer.
