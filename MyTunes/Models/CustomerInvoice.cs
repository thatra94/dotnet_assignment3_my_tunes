﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyTunes.Models
{
    // Model for customer invoice with Id and Total spent 
    public class CustomerInvoice
    {
        public int CustomerId { get; set; }
        public decimal Total { get; set; }
    }
}

