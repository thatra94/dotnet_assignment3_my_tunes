﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyTunes.Models
{
    // Model for artist with name and ID
    public class Artist
    {
        public int ArtistId { get; set; }
        public string Name { get; set; }

    }
}
