﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyTunes.Models
{
    // Model for Genre with name and ID
    public class Genre
    {
        public int GenreId { get; set; }
        public string Name { get; set; }

    }
}
