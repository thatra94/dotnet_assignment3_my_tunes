﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyTunes.Models
{
    // Model for country with name and number of customers
    public class Country
    {
        public string CountryName { get; set; }

        public int CustomerCount { get; set; }
    }
}
