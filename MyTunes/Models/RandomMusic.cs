﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyTunes.Models
{
    // Model for random music with arrays of Music, Artists and Genres
    public class RandomMusic
    {
        public Music[] Tracks { get; set; }
        public Artist[] Artists { get; set; }
        public Genre[] Genres { get; set; }
    }
}
