﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyTunes.Models
{
    // Model music songs with Id and Name
    public class Music
    {
        public int TrackId { get; set; }
        public string Name { get; set; }

    }
}
