﻿using MyTunes.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;


namespace MyTunes.Repositories
{
    public class CustomerRepository
    {

        // Get all customers
        public List<Customer> GetAllCustomers()
        {
   
            // Makes a list of customers
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while(reader.Read())
                            {
                                // Temporary customer that we map data from database to and add to customerlist
                                // Also check if column is null
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? null : reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                customerList.Add(temp);
                            }
                        }
                    }
                }
            } catch (SqlException ex)
            {
                // Log to console
                Console.WriteLine(ex.Message);

            }
         
            return customerList;
        }

        public List<CustomerPopularGenre> GetCustomerPopularGenre(int id)
        {
            List<CustomerPopularGenre> favouriteGenre = new List<CustomerPopularGenre>();

            // Joins the tables invoice, invoiceLine, Track and Genre
            // Counts the number of invoices the customer has to a Genre
            // Returns the genre with most invoices and returns the tied genres if there is any

            string sql = "SELECT TOP 1 WITH TIES COUNT(Genre.GenreId) as genreCount, Genre.Name FROM Invoice" +
                " JOIN InvoiceLine ON InvoiceLine.InvoiceId = Invoice.InvoiceId" +
                " JOIN Track ON Track.TrackId = InvoiceLine.TrackId" +
                " JOIN Genre ON Genre.GenreId = Track.GenreId WHERE CustomerId = @CustomerId" +
                " GROUP BY Genre.Name ORDER BY genreCount DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerPopularGenre temp = new CustomerPopularGenre();
                                temp.InvoiceCounts = reader.GetInt32(0);
                                temp.Genre = reader.GetString(1);
                                favouriteGenre.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log to console
                Console.WriteLine(ex.Message);
            }

            return favouriteGenre;
        }

        // Get all customer by a specific country
        public List<Customer> GetAllCustomersByCountry(string country)
        {
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" + $" WHERE Country = @Country";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@Country", country);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? null : reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                customerList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log to console
                Console.WriteLine(ex.Message);
            }

            return customerList;
        }

        // Counting all customers in all the countries then return them ordered by amount of customers ordered descending
        public List<Country> GetAllCustomersOrderedByCountry()
        {
            List<Country> countryList = new List<Country>();
            string sql = "SELECT Country, COUNT(*) FROM Customer GROUP BY Country ORDER BY COUNT(*) DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Country temp = new Country();
                                temp.CountryName = reader.GetString(0);
                                temp.CustomerCount = reader.GetInt32(1);
                                countryList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log to console
                Console.WriteLine(ex.Message);

            }

            return countryList;
        }


        // Returns the sum of invoices total for customers
        // Orders by the customers with highest total in invoice to least
        public List<CustomerInvoice> GetAllCustomersOrderedByInvoices()
        {
            List<CustomerInvoice> customerInvoicesList = new List<CustomerInvoice>();
            string sql = "SELECT CustomerId, SUM(Total) AS Total FROM Invoice GROUP BY CustomerId ORDER BY Total DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerInvoice temp = new CustomerInvoice();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.Total = reader.GetDecimal(1);
                                customerInvoicesList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log to console
                Console.WriteLine(ex.Message);

            }

            return customerInvoicesList;
        }


        // Get a customer with a specific ID
        public Customer GetCustomer(int id)
        {
            Customer customer= new Customer();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" + " WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.GetString(4);
                                customer.Phone = reader.GetString(5);
                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log to console
                Console.WriteLine(ex.Message);
            }

            return customer;
        }

        // Takes in a customer object then adds it to the database
        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;

            string sql = "INSERT INTO Customer(FirstName,LastName,Country, PostalCode, Phone, Email) " +
                "VALUES(@FirstName,@LastName,@Country, @PostalCode, @Phone, @Email)";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log to console
                Console.WriteLine(ex.Message);

            }
            return success;
        }

        // Updates a customer in the database, returns false if customer does not exist
        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            string sql = "UPDATE Customer SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email" + 
                " WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    { 
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log to console
                Console.WriteLine(ex.Message);
            }
            return success;
        }
    }
}
