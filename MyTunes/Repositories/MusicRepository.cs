﻿using MyTunes.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
namespace MyTunes.Repositories
{
    public class MusicRepository
    {
        // Get music by ID
        public Music GetMusic(int id)
        {

            Music music = new Music();
            string sql = "SELECT TrackId, Name FROM Track" + " WHERE TrackId = @TrackId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {

                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@TrackId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                music.TrackId = reader.GetInt32(0);
                                music.Name = reader.GetString(1);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log to console
                Console.WriteLine(ex.Message);
            }

            return music;
        }

        // Get track by trackname
        public Track GetTrack(string trackName)
        {
            Track responseTrack = new Track();

            // Joins the tables Track, Album, Artist, Genre
            // Gets track name, artist name, album title and genre name by track name
            string sql = "SELECT Track.Name, Artist.Name, Album.Title, Genre.Name FROM Track" +
                " INNER JOIN Album ON Album.AlbumId = Track.AlbumId" +
                " INNER JOIN Artist ON Artist.ArtistId = Album.ArtistId" +
                " INNER JOIN Genre ON Genre.GenreId = Track.GenreId" +
                " WHERE Track.Name = @TrackName";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {

                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@TrackName", trackName);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                responseTrack.TrackName = reader.GetString(0);
                                responseTrack.Artist = reader.GetString(1);
                                responseTrack.Album = reader.GetString(2);
                                responseTrack.Genre = reader.GetString(3);
                            }
                        }

                    }
                }
            }
            catch (SqlException ex)
            {
                // Log to console
                Console.WriteLine(ex.Message);
            }

            return responseTrack;
        }


        // Get artist by id
        public Artist GetArtist(int id)
        {
            Artist artist = new Artist();

            string sql = "SELECT ArtistId, Name FROM Artist" + " WHERE ArtistId = @ArtistId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {

                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@ArtistId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                artist.ArtistId = reader.GetInt32(0);
                                artist.Name = reader.GetString(1);
                            }
                        }

                    }
                }
            }
            catch (SqlException ex)
            {
                // Log to console
                Console.WriteLine(ex.Message);
            }

            return artist;
        }



        // Get random music
        public List<Music> GetTrackRandom()
        {
            
            List<Music> musicList = new List<Music>();
            
            // Get 5 random music with trackId, Name, and composer
            string sql = "SELECT TOP 5 TrackId, Name, Composer FROM Track " +
                         "ORDER BY NEWID()";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {

                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Music music = new Music();
                                music.TrackId = reader.GetInt32(0);
                                music.Name = reader.GetString(1);
                                musicList.Add(music);

                            }
                        }

                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                // Log to console
            }

            return musicList;
        }

        // Get 5 random artists
        public List<Artist> GetArtistRandom()
        {
            List<Artist> artistList = new List<Artist>();

            // Get 5 random artists with artistId and Name
            string sql = "SELECT TOP 5 ArtistId, Name FROM Artist " +
                         "ORDER BY NEWID()";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {

                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Artist artist = new Artist();
                                artist.ArtistId = reader.GetInt32(0);
                                artist.Name = reader.GetString(1);
                                artistList.Add(artist);
                                Console.WriteLine(artist);

                            }
                        }

                    }
                }
            }
            catch (SqlException ex)
            {
                // Log to console
                Console.WriteLine(ex.Message);
            }

            return artistList;
        }

        
        // Get random genres
        public List<Genre> GetGenreRandom()
        {
            List<Genre> genreList = new List<Genre>();

            // Get 5 random genres with genreId and name
            string sql = "SELECT TOP 5 GenreId, Name FROM Genre " +
                         "ORDER BY NEWID()";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {

                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Genre genre = new Genre();
                                genre.GenreId = reader.GetInt32(0);
                                genre.Name = reader.GetString(1);
                                genreList.Add(genre);
                                Console.WriteLine(genre);

                            }
                        }

                    }
                }
            }
            catch (SqlException ex)
            {
                // Log to console
                Console.WriteLine(ex.Message);
            }

            return genreList;
        }

        // Get random combination of music, artist and genre
        public RandomMusic GetRandomMusic()
        {
            
            // Get list of genres, artists and music
            List<Genre> genreList = GetGenreRandom();
            List<Artist> artistList = GetArtistRandom();
            List<Music> musicList = GetTrackRandom();

            // random music model
            RandomMusic randomMusic = new RandomMusic();

            // Aggregate the data
            randomMusic.Genres = genreList.ToArray();
            randomMusic.Artists = artistList.ToArray();
            randomMusic.Tracks = musicList.ToArray();

            return randomMusic;
        }

        // Get genre by ID
        public Genre GetGenre(int id)
        {
            Genre genre = new Genre();

            // Get genreId and name by ID
            string sql = "SELECT GenreId, Name FROM Genre" + " WHERE GenreId = @GenreId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {

                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@GenreId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                genre.GenreId = reader.GetInt32(0);
                                genre.Name = reader.GetString(1);
                            }
                        }

                    }
                }
            }
            catch (SqlException ex)
            {
                // Log to console
                Console.WriteLine(ex.Message);
            }

            return genre;
        }
    }
}
