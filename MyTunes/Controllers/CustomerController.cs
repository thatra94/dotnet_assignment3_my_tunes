﻿using Microsoft.AspNetCore.Mvc;
using MyTunes.Models;
using MyTunes.Repositories;
using System.Collections.Generic;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MyTunes.Controllers
{
    [Route("api/customers")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly CustomerRepository _customerRepository;
        public CustomerController(CustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        // GET api/customers
        // GET api/customers?country=brazil
        [HttpGet]
        public ActionResult<Customer> GetCustomers([FromQuery] string country)
        {
            if (country != null)
            {
                return Ok(_customerRepository.GetAllCustomersByCountry(country));
            }
            return Ok(_customerRepository.GetAllCustomers());
        }

        // GET api/customers/5
        [HttpGet("{id}")]
        public ActionResult<Customer> GetCustomerById([FromRoute] int id)
        {
            try
            {
                return Ok(_customerRepository.GetCustomer(id));
            }
            catch
            {
                return NotFound();
            }
        }

        // GET api/customers/countries
        [HttpGet("countries")]
        public ActionResult<Country> GetCustomerCountries()
        {
            return Ok(_customerRepository.GetAllCustomersOrderedByCountry());

        }

        // GET api/customers/payments
        [HttpGet("payments")]
        public ActionResult<CustomerInvoice> GetPayments()
        {
            return Ok(_customerRepository.GetAllCustomersOrderedByInvoices());

        }

        // GET api/customers/5/genres
        [HttpGet("{id}/genres/popular")]
        public ActionResult<CustomerPopularGenre> GetCustomerPopularGenre([FromRoute] int id) {

            try
            {
                return Ok(_customerRepository.GetCustomerPopularGenre(id));
            } catch
            {
                return NotFound();
            }
        }

        //POST api/customers
        [HttpPost]
        public ActionResult PostCustomer([FromBody] Customer customer)
        {
            // Going to use our CustomerRepository to add a new customer
            bool success = _customerRepository.AddNewCustomer(customer);
            if (success == false)
            {
                return BadRequest();
            }
            return CreatedAtAction(nameof(GetCustomerById), new { id = customer.CustomerId }, success);
        }



        // PUT api/customers/5
        [HttpPut("{id}")]
        public ActionResult PutCustomerById([FromRoute] int id, [FromBody] Customer customer)
        {
            // Check for bad request
            if(id != customer.CustomerId)
            {
                return BadRequest();
            }
            bool success = _customerRepository.UpdateCustomer(customer);
            if(success)
            {
               return NoContent();
            }
            return NotFound();
        }
    }
}
