using Microsoft.AspNetCore.Mvc;
using MyTunes.Models;
using MyTunes.Repositories;
using System.Collections.Generic;

namespace MyTunes.Controllers
{
    [Route("api/music")]
    [ApiController]
    public class MusicController : ControllerBase
    {
        private readonly MusicRepository _musicRepository;
        public MusicController(MusicRepository musicRepository)
        {
            _musicRepository = musicRepository;
        }

        // GET api/music/5
        [HttpGet("track/{id}")]

        public ActionResult<Music> GetTrack([FromRoute] int id)
        {
            try
            {
                return Ok(_musicRepository.GetMusic(id));
            }
            catch
            {
                return NotFound();
            }
        }

        // GET api/music/genre/5
        [HttpGet("genre/{id}")]
        public ActionResult<Genre> GetGenre([FromRoute] int id)
        {
            try
            {
                return Ok(_musicRepository.GetGenre(id));
            } 
            catch
            {
                return NotFound();
            }
        }

        // GET api/music/artist/5
        [HttpGet("artist/{id}")]
        public ActionResult<Artist> GetArtist([FromRoute] int id)
        {
            try
            {
                return Ok(_musicRepository.GetArtist(id));
            }
            catch
            {
                return NotFound();
            }
        }

        // GET api/music/home
        [HttpGet("home")]
        public ActionResult<RandomMusic> GetRandomMusic()
        {
            return Ok(_musicRepository.GetRandomMusic());
        }

        // GET api/music/search?track=foo
        [HttpGet("search")]
        public ActionResult<Artist> GetTrackSearch([FromQuery] string track)
        {
            if (track == null)
            {
                return BadRequest();
            }
            try
            {
                return Ok(_musicRepository.GetTrack(track));
            }
            catch
            {
                return NotFound();
            }
        }
    }
}
